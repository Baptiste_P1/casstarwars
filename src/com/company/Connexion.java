package com.company;

import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexion {

    public Connexion(){
        try {
            String strClassName = "com.mysql.jdbc.Driver";
            String dbName = "tpjdbc";
            String login = "root";
            String motdepasse = "";
            String strUrl = "jdbc:mysql://localhost:3306/" + dbName;


            Class.forName(strClassName);
            java.sql.Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);

            conn.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Driver non chargé !");
            e.printStackTrace();
        }
    }
}
