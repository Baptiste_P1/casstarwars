package com.company;

import java.util.Vector;

public class Acteur {
    private String NomActeur;
    private String PrenomActeur;
    private Vector<Personnage> ListePersonnage = new Vector();



    public Acteur(String NomActeur, String PrenomActeur,Vector<Personnage> ListePersonnage ){
        this.setNomActeur(NomActeur);
        this.setPrenomActeur(PrenomActeur);
        this.setListePersonnage(ListePersonnage);
    }
    public Acteur(){

    }


    public String getNomActeur() {
        return NomActeur;
    }

    public void setNomActeur(String nomActeur) {
        NomActeur = nomActeur;
    }

    public String getPrenomActeur() {
        return PrenomActeur;
    }

    public void setPrenomActeur(String prenomActeur) {
        PrenomActeur = prenomActeur;
    }

    public Vector<Personnage> getListePersonnage() {
        return ListePersonnage;
    }

    public void setListePersonnage(Vector<Personnage> listePersonnage) {
        ListePersonnage = listePersonnage;
    }

    @Override
    public String toString() {
        return "Acteur{" +
                "NomActeur='" + NomActeur + '\'' +
                ", PrenomActeur='" + PrenomActeur + '\'' +
                ", ListePersonnage=" + ListePersonnage +
                '}';
    }
}
