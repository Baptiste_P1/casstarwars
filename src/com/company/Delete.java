package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Delete {

    //Ecrire une méthode main dans une classe qui permet de supprimer des lignes dans la table dont l’identifiant est demandé à l’utilisateur
    public static void main(String[] args) {

        try {
            String strClassName = "com.mysql.jdbc.Driver";
            String dbName = "tpjdbc";
            String login = "root";
            String motdepasse = "";
            String strUrl = "jdbc:mysql://localhost:3306/" + dbName;
            Scanner sc = new Scanner(System.in);
            System.out.println("Entrez l'id de l'utilisateur: ");
            int idUser = sc.nextInt();
            sc.nextLine();


            Class.forName(strClassName);
            Connection conn = DriverManager.getConnection(strUrl, login, motdepasse);
            Statement state =  (Statement) conn.createStatement();
            state.executeUpdate("DELETE FROM acces WHERE id=\""+ idUser+"\"");
            state.close();

            conn.close();
        } catch (ClassNotFoundException e) {
            System.err.println("Driver non chargé !");
            e.printStackTrace();
        } catch (SQLException e) {
            System.err.println("Autre erreur !");
            e.printStackTrace();
        }
    }

}


