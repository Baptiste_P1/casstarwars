package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

public class Film {
    private String Titre;
    private String AnneeDeSortie;
    private int numero;
    private int Cout;
    private int recette;
    ArrayList<Acteur> ListeActeurs = new ArrayList();



    public Film (String Titre, String AnnneeDeSortie, int numero,  int Cout, int Recette, ArrayList<Acteur> ListeActeurs){
        this.setTitre(Titre);
        this.setAnneeDeSortie(AnnneeDeSortie);
        this.setCout(Cout);
        this.setRecette(Recette);
        this.setListeActeurs(ListeActeurs);
        this.setNumero(numero);
    }
    public Film(){

    }
    public ArrayList<Acteur> getListeActeurs() {
        return ListeActeurs;
    }

    public void setListeActeurs(ArrayList<Acteur> listeActeurs) {
        ListeActeurs = listeActeurs;
    }

    public String getAnneeDeSortie() {
        return AnneeDeSortie;
    }

    public void setAnneeDeSortie(String anneeDeSortie) {
        AnneeDeSortie = anneeDeSortie;
    }

    public String getTitre() {
        return Titre;
    }

    public void setTitre(String titre) {
        Titre = titre;
    }

    public int getCout() {
        return Cout;
    }

    public void setCout(int cout) {
        Cout = cout;
    }

    public int getRecette() {
        return recette;
    }

    public void setRecette(int recette) {
        this.recette = recette;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Film{" +
                "Titre='" + Titre + '\'' +
                ", AnneeDeSortie='" + AnneeDeSortie + '\'' +
                ", numero=" + numero +
                ", Cout=" + Cout +
                ", recette=" + recette +
                ", ListeActeurs=" + ListeActeurs +
                '}';
    }

    public int nbActeurs() {
        return this.ListeActeurs.size();
    }

    public int nbPersonnages(){
        int b = 0;
        for (Acteur a :ListeActeurs
             ) {
             b += a.getListePersonnage().size();
        }
        return b;
    }
/*    public ArrayList<List> calculBenefice(){
        boolean estBenefice = false;
        if(this.getCout() < this.getRecette()){
            estBenefice = true;
        }
        int benefice = this.getCout() - this.getRecette();
        ArrayList<List> listeFilm = new ArrayList<List>();
        ArrayList<Integer> benefices = new ArrayList<>(Integer);
        ArrayList<Boolean> estBenefices = new ArrayList<>(Boolean);
        benefices.add(benefice);
        estBenefices.add(estBenefice);
        listeFilm.add(benefices);
        listeFilm.add(estBenefices);
        return  listeFilm;

    }*/

    public boolean isBefore(String dateSortie){
        boolean bool = false;
        if(this.getAnneeDeSortie().equals(dateSortie)){
            bool = true;
        }
        return bool;
    }

    public void tri(){
        Collections.sort(ListeActeurs, new Comparator<Acteur>() {
            public int compare(Acteur v1, Acteur v2) {
                return v1.getNomActeur().compareTo(v2.getNomActeur());
            }
        });
    }

}
