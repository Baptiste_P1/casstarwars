package com.company;

public class Personnage {
    private String NomPersonnage;
    private String PrenomPersonnage;

    public Personnage(String NomPersonnage, String PrenomPersonnage){
        this.setNomPersonnage(NomPersonnage);
        this.setPrenomPersonnage(PrenomPersonnage);
    }

    public Personnage() {
    }

    public String getNomPersonnage() {
        return NomPersonnage;
    }

    public void setNomPersonnage(String nomPersonnage) {
        NomPersonnage = nomPersonnage;
    }

    public String getPrenomPersonnage() {
        return PrenomPersonnage;
    }

    public void setPrenomPersonnage(String prenomPersonnage) {
        PrenomPersonnage = prenomPersonnage;
    }


}
