package com.company;
import com.company.Film;

import java.sql.*;
import java.sql.Connection;
import java.util.ArrayList;


import java.util.Scanner;

public class DAOFilm extends Connexion {
    public DAOFilm()  {
        super();
    }

    public boolean delete(Film obj){
        try {
            Connection conn = this.connect;
            Scanner sc = new Scanner(System.in);
            System.out.println("Entrez l'id du Film: ");
            int idFilm = sc.nextInt();
            sc.nextLine();
            Statement state =  (Statement) conn.createStatement();
            state.executeUpdate("DELETE FROM acces WHERE id=\""+ idFilm+"\"");
            state.close();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }
    public boolean ajouter(Film obj){
        try {
            Connection conn = this.connect;
            PreparedStatement state = conn.prepareStatement("INSERT INTO film(id, titre, anneeSortie, numeroEpisode, recette, cout) " +
                    "VALUES(?, ?, ?, ?, ?, ?)");


            state.setInt(1, obj.getId());
            state.setString(2,obj.getTitre());
            state.setString(3, obj.getAnneeDeSortie());
            state.setInt(4, obj.getNumero());
            state.setInt(5, obj.getRecette());
            state.setInt(6, obj.getCout());



        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    public boolean lister(Film obj){
        try{
            Connection conn = this.connect;
            Statement stUsers = conn.createStatement();

            ResultSet rsUsers = stUsers.executeQuery("select * from Film");
            while(rsUsers.next()) {
                System.out.print("Id[" + rsUsers.getInt(1) + "]"
                        + rsUsers.getString(2)
                        + "[" + rsUsers.getString("statut") + "] "
                        + rsUsers.getInt("age") +"\n"); }
            conn.close();
        }
     catch (SQLException throwables) {
        throwables.printStackTrace();
    }

        return false;
    }

}

